下記環境で作業したフィードバックです。
OS：Windows7
IDE:Eclipse4.5

・実際のサービスでは対応予定のものや特に問題ないものも含まれているかもしれませんが
　せっかくのテスト運用ということですので、気がついた事を記載しておきます。
　※新人向けという観点で記載しております。

■誤字について
【会議室予約】実装ガイド.pdf 7P
得には　→　特には

■環境設定について
１．Proxy
・BtoBサービスということで、個人ではなく企業がサービス利用することを考えると
　gradleの初期設定でProxyについて触れている方が、より親切ではないかな？と感じました。
例：
Proxyサーバを導入しているしている場合は、「gradle.properties」にProxyの定義をする。
systemProp.http.proxyHost=proxy.kronos-jp.net
systemProp.http.proxyPort=8080
systemProp.http.proxyUser=userid
systemProp.http.proxyPassword=password
systemProp.https.proxyHost=proxy.kronos-jp.net
systemProp.https.proxyPort=8080
systemProp.https.proxyUser=userid
systemProp.https.proxyPassword=password

２．build.gradle
・compile 'javax.servlet:jstl:1.2'の記述が２箇所ありました。

・私の環境では、JSPで以下のエラーが表示されていました。（Eclipseで表示されるだけで動作はする）
　　- javax.servlet.jsp.PageContext を型に解決できません
　　- javax.servlet.jsp.JspException を型に解決できません
　　下記設定を追加で対応しました。
　　compile 'javax.servlet.jsp:jsp-api:2.2'

３．環境に合わせてDBパスワードを変更する必要性がある旨の記述を
　　「【会議室予約】実装ガイド.pdf」に記載しておいたほうが親切かなと感じました。

４．サーバ起動後にＷｅｂアプリケーションにアクセスするためのＵＲＬを
　　「【会議室予約】実装ガイド.pdf」に記載しておいたほうが親切かなと感じました。

■ソースコードについて
１．jspファイルにタブとスペースが混在しているものがいくつかありました。
　　全てスペース４桁or２桁もしくはタブにして、統一性を持たせて置いたほうが良いように感じました。

２．jspファイル内のサイズ指定などで、
　　単位がpxだったりemだったりと統一されていないところがありましたので
　　特別な意味がないのであれば、統一したほうが良いように感じました。（個人的にはpxが好き）
　　
３．crr.cssを読み込んでいるのに、タグにstyle属性で色々指定していましたので
　　スタイル関連はCSSファイルにまとめた方が良いように感じました。
　　
４．desktop.ini の必要性が分かりませんでした。（GoogleDrive関連？）
　　不要であれば、ソース内から取り除いておいたほうが良さそうです。

５．入力チェックに使っているグループインタフェースの名前が誤字？のように感じました。
　　ValidathiontGroup
　　↓
　　ValidationGroup
　　
６．ValidationMessages_ja.propertiesのlengthチェックのメッセージが
　　最大値と最小値でテレコになっているようなので修正しました。
　　【修正前】
　　org.hibernate.validator.constraints.Length.min.message={0}は{min}文字以内で入力して下さい。
　　org.hibernate.validator.constraints.Length.max.message={0}は{max}文字以上で入力して下さい。

　　【修正後】
　　org.hibernate.validator.constraints.Length.min.message={0}は{min}文字以上で入力して下さい。
　　org.hibernate.validator.constraints.Length.max.message={0}は{max}文字以内で入力して下さい。

■設計書関連
１．【会議室予約】会議室編集 基本設計書.pdf　P5
　　保存ボタン押下時のValidate記述にて、以下文言が不明
　　「・画面項目の文字種別に違反していないかチェック。」←具体的に何をチェックする分かりませんでした。
　　「・終了日時が開始日時以前になっていないかチェック」←誤記？

■その他
　　ほとんどソースを流用するので、技術的にあまり伸びないのかなと最初は思いましたが、
　　参考になるソースがあって調べる時間が短縮できて、効率的に学習できました。
　　また、流用するためにも理解しておく必要があるため、ソースを読むことで学習にもつながりました。
　　
　　懸念する点としては、ソースコードレビューや質問への回答をする側の負荷がかかるので
　　何かうまく負荷を減らす工夫が必要ではないかと感じました。
