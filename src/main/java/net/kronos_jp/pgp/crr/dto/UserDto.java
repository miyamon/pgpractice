package net.kronos_jp.pgp.crr.dto;

import net.kronos_jp.pgp.crr.entity.Users;

public class UserDto extends Users {

	private static final long serialVersionUID = -8415177255399630867L;

	String roleName;

	String role;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
