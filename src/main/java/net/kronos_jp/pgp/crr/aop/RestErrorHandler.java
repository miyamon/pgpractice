package net.kronos_jp.pgp.crr.aop;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * リクエストで入力エラーが存在した際のハンドリングをし、エラーメッセージを返却します。
 * 
 * 指定ではController呼び出し時全てに適用されるが、
 * 現状はRESTリクエスト時しか入力エラーが存在しない。
 * 今後RESTのみに適用する場合は@ControllerAdvice(annotations = RestController.class)を指定すること。
 */
@ControllerAdvice
public class RestErrorHandler extends ResponseEntityExceptionHandler {

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);

		List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
		List<ObjectError> globalErrors = e.getBindingResult().getGlobalErrors();
		List<String> errors = new ArrayList<>(fieldErrors.size() + globalErrors.size());
		for (FieldError fieldError : fieldErrors) {
			errors.add(MessageFormat.format(fieldError.getDefaultMessage(), message.getMessage(fieldError.getField())));
		}
		for (ObjectError objectError : globalErrors) {
			errors.add(MessageFormat.format(objectError.getDefaultMessage(),
					message.getMessage(objectError.getObjectName())));
		}
		return new ResponseEntity<Object>(errors, headers, status);

	}
}
