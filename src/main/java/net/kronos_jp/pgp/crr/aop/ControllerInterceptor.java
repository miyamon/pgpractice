package net.kronos_jp.pgp.crr.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerInterceptor {

	// private static final Logger logger = LoggerFactory.getLogger(ControllerInterceptor.class);

	@Autowired
	Environment env;

	public Object init(ProceedingJoinPoint pjp) throws Throwable {

		Object result = pjp.proceed();

		return result;
	}
}
