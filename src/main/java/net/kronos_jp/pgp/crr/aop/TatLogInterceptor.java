package net.kronos_jp.pgp.crr.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * TATログを出力します。
 * 
 * 運用ではWANにすることで、ログを抑止する。
 */
@Aspect
@Component
public class TatLogInterceptor {

	private static final Logger logger = LoggerFactory.getLogger("tat");

	public Object tatDoFilter(ProceedingJoinPoint pjp) throws Throwable {
		long start = System.currentTimeMillis();
		try {
			return pjp.proceed();
		} finally {
			if (logger.isInfoEnabled()) {
				long end = System.currentTimeMillis();
				String className = pjp.getSignature().getDeclaringType().getName();
				String methodName = pjp.getSignature().getName();
				logger.info("Method [" + className + "#" + methodName + "] Start [" + start + "] End [" + end
						+ "] Time [" + (end - start) + "]");
			}
		}
	}
}
