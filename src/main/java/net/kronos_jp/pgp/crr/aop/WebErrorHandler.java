package net.kronos_jp.pgp.crr.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * 画面でエラーが起きた場合の対応。ログ出力。エラー画面への遷移。
 * これでもとらえきれない例外（Spring自体がおかしい等）の場合はweb.xmlで設定する500の画面へ遷移。
 */
@Component
public class WebErrorHandler implements HandlerExceptionResolver {

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	private static final Logger logger = LoggerFactory.getLogger(WebErrorHandler.class);

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object obj,
			Exception ex) {
		logger.error(ex.getMessage(), ex.getCause() == null ? ex : ex.getCause());

		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);

		ModelAndView mav = new ModelAndView();
		mav.addObject("error", message.getMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString()));
		mav.setViewName("error");

		return mav;
	}
}
