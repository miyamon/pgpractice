package net.kronos_jp.pgp.crr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import net.kronos_jp.pgp.crr.dto.UserDto;
import net.kronos_jp.pgp.crr.entity.Users;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * {@link Users}用のDAO。
 */
@Component
public class UsersDao extends AbstractDao {

	/**
	 * idで検索します。※論理削除されたデータは含まれない。
	 * 
	 * @param id
	 *            id
	 * @return 検索結果
	 */
	public UserDto selectById(Integer id) {

		try {
			return jdbcTemplate
					.queryForObject(
							"SELECT users.id, users.account, users.name, users.password, users.role_id, users.created_user_id, users.updated_user_id, users.created_at, users.updated_at, users.deleted_at, roles.name AS role_name, roles.role "
									+ "FROM users INNER JOIN roles ON users.role_id = roles.id WHERE users.id = ? AND users.deleted_at IS NULL AND roles.deleted_at IS NULL",
							new RowMapper<UserDto>() {
								@Override
								public UserDto mapRow(ResultSet rs, int rowNum) throws SQLException {
									UserDto result = new UserDto();
									result.setId(rs.getInt("id"));
									result.setAccount(rs.getString("account"));
									result.setName(rs.getString("name"));
									result.setPassword(rs.getString("password"));
									result.setRoleId(rs.getInt("role_id"));
									result.setCreatedUserId(rs.getInt("created_user_id"));
									result.setUpdatedUserId(rs.getInt("updated_user_id"));
									result.setCreatedAt(rs.getTimestamp("created_at"));
									result.setUpdatedAt(rs.getTimestamp("updated_at"));
									result.setRole(rs.getString("role"));
									result.setRoleName(rs.getString("role_name"));
									return result;
								}
							}, id);

		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * アカウントで検索します。※論理削除されたデータは含まれない。
	 * 
	 * @param account
	 *            アカウント
	 * @return 検索結果
	 */
	public UserDto selectByAccount(String account) {

		try {
			return jdbcTemplate
					.queryForObject(
							"SELECT users.id, users.account, users.name, users.password, users.role_id, users.created_user_id, users.updated_user_id, users.created_at, users.updated_at, users.deleted_at, roles.name AS role_name, roles.role "
									+ "FROM users INNER JOIN roles ON users.role_id = roles.id WHERE users.account = ? AND users.deleted_at IS NULL AND roles.deleted_at IS NULL",
							new RowMapper<UserDto>() {
								@Override
								public UserDto mapRow(ResultSet rs, int rowNum) throws SQLException {
									UserDto result = new UserDto();
									result.setId(rs.getInt("id"));
									result.setAccount(rs.getString("account"));
									result.setName(rs.getString("name"));
									result.setPassword(rs.getString("password"));
									result.setRoleId(rs.getInt("role_id"));
									result.setCreatedUserId(rs.getInt("created_user_id"));
									result.setUpdatedUserId(rs.getInt("updated_user_id"));
									result.setCreatedAt(rs.getTimestamp("created_at"));
									result.setUpdatedAt(rs.getTimestamp("updated_at"));
									result.setRole(rs.getString("role"));
									result.setRoleName(rs.getString("role_name"));
									return result;
								}
							}, account);

		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 全てのユーザー情報を取得します。※論理削除されたデータは含まれない。
	 * 
	 * @return 全てのユーザー
	 */
	public List<UserDto> listAll() {
		return jdbcTemplate
				.query("SELECT users.id, users.account, users.name, users.password, users.role_id, users.created_user_id, users.updated_user_id, users.created_at, users.updated_at, users.deleted_at, roles.name AS role_name, roles.role "
						+ "FROM users INNER JOIN roles ON users.role_id = roles.id WHERE users.deleted_at IS NULL AND roles.deleted_at IS NULL ORDER BY users.id",
						new RowMapper<UserDto>() {
							@Override
							public UserDto mapRow(ResultSet rs, int rowNum) throws SQLException {
								UserDto result = new UserDto();
								result.setId(rs.getInt("id"));
								result.setAccount(rs.getString("account"));
								result.setName(rs.getString("name"));
								result.setPassword(rs.getString("password"));
								result.setRoleId(rs.getInt("role_id"));
								result.setCreatedUserId(rs.getInt("created_user_id"));
								result.setUpdatedUserId(rs.getInt("updated_user_id"));
								result.setCreatedAt(rs.getTimestamp("created_at"));
								result.setUpdatedAt(rs.getTimestamp("updated_at"));
								result.setRole(rs.getString("role"));
								result.setRoleName(rs.getString("role_name"));
								return result;
							}
						});

	}

	/**
	 * 指定されたアカウントが重複していないかカウントする。
	 * 
	 * @param account
	 *            アカウント
	 * @return 存在する数
	 */
	public Integer countAccount(String account) {
		return jdbcTemplate.queryForObject("SELECT COUNT(id) FROM users WHERE account = ?", Integer.class, account);
	}

	/**
	 * インサートする
	 * 
	 * @param account
	 *            アカウント
	 * @return 存在する数
	 */
	public int insert(Users users) {
		return jdbcTemplate
				.update("INSERT INTO users (account, name, password, role_id, created_user_id, updated_user_id, created_at, updated_at, deleted_at ) VALUES (?,?,?,?,?,?,?,?,?)",
						users.getAccount(), users.getName(), users.getPassword(), users.getRoleId(),
						users.getCreatedUserId(), users.getUpdatedUserId(), users.getCreatedAt(), users.getUpdatedAt(),
						users.getDeletedAt());
	}

	/**
	 * 更新する 更新日付で楽観ロック。
	 * 
	 * @param lock
	 *            楽観ロックに使用する更新日時
	 * @return 更新件数
	 */
	public int update(Users users, Timestamp lock) {

		return jdbcTemplate
				.update("UPDATE users SET account = ?, name = ?, password = ?, role_id = ?, updated_user_id = ?, updated_at = ?, deleted_at = ? "
						+ "WHERE id = ? AND updated_at = ?", users.getAccount(), users.getName(), users.getPassword(),
						users.getRoleId(), users.getUpdatedUserId(), users.getUpdatedAt(), users.getDeletedAt(),
						users.getId(), lock);
	}
}
