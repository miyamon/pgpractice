package net.kronos_jp.pgp.crr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import net.kronos_jp.pgp.crr.dto.RoomDto;
import net.kronos_jp.pgp.crr.entity.Rooms;

/**
 * {@link Rooms}用のDAO。
 */
@Component
public class RoomsDao extends AbstractDao {

	/**
	 * 全ての会議室情報を取得する。※論理削除されたデータは含まれない。
	 *
	 * @return 検索結果
	 */
	public List<RoomDto> listAll() {
		StringBuilder sql = new StringBuilder();
		sql.append(createCommonSelectSQL());
		sql.append("FROM");
		sql.append("  rooms ");
		sql.append("WHERE");
		sql.append("  deleted_at IS NULL ");
		sql.append("ORDER BY");
		sql.append("  id");

		return jdbcTemplate.query(sql.toString(), new RowMapper<RoomDto>() {
			@Override
			public RoomDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				RoomDto result = new RoomDto();
				result.setId(rs.getInt("id"));
				result.setName(rs.getString("name"));
				result.setCreatedUserId(rs.getInt("created_user_id"));
				result.setUpdatedUserId(rs.getInt("updated_user_id"));
				result.setCreatedAt(rs.getTimestamp("created_at"));
				result.setUpdatedAt(rs.getTimestamp("updated_at"));
				return result;
			}
		});
	}

	/**
	 * 会議室情報をidで検索する。※論理削除されたデータは含まれない。
	 *
	 * @return 検索結果
	 */
	public RoomDto selectById(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append(createCommonSelectSQL());
		sql.append("FROM");
		sql.append("  rooms ");
		sql.append("WHERE");
		sql.append("  id = ?");
		sql.append("  AND deleted_at IS NULL ");
		sql.append("ORDER BY");
		sql.append("  id");

		return jdbcTemplate.queryForObject(sql.toString(), new RowMapper<RoomDto>() {
			@Override
			public RoomDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				RoomDto result = new RoomDto();
				result.setId(rs.getInt("id"));
				result.setName(rs.getString("name"));
				result.setCreatedUserId(rs.getInt("created_user_id"));
				result.setUpdatedUserId(rs.getInt("updated_user_id"));
				result.setCreatedAt(rs.getTimestamp("created_at"));
				result.setUpdatedAt(rs.getTimestamp("updated_at"));
				return result;
			}
		}, id);
	}

	/**
	 * 会議室情報を登録する。
	 *
	 * @param rooms
	 *            登録する会議室情報
	 * @return 登録件数
	 */
	public int insert(Rooms rooms) {
		return jdbcTemplate.update(
				"INSERT INTO rooms (name, created_user_id, updated_user_id, created_at, updated_at ) VALUES (?,?,?,?,?)",
				rooms.getName(), rooms.getCreatedUserId(), rooms.getUpdatedUserId(), rooms.getCreatedAt(),
				rooms.getUpdatedAt());
	}

	/**
	 * 会議室情報を更新する。 更新日付で楽観ロック。
	 *
	 * @param rooms
	 *            更新する会議室情報
	 * @param lock
	 *            楽観ロックに使用する更新日時
	 * @return 更新件数
	 */
	public int update(Rooms rooms, Timestamp lock) {

		return jdbcTemplate.update(
				"UPDATE rooms SET name = ?, updated_user_id = ?, updated_at = ?, deleted_at = ? WHERE id = ? AND updated_at = ?",
				rooms.getName(), rooms.getUpdatedUserId(), rooms.getUpdatedAt(), rooms.getDeletedAt(), rooms.getId(),
				lock);
	}

	/**
	 * 会議室検索SQLの共通部分を作成する。
	 *
	 * @return 会議室検索SQLの共通部分
	 */
	private String createCommonSelectSQL() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT");
		sql.append("  id,");
		sql.append("  name,");
		sql.append("  created_user_id,");
		sql.append("  updated_user_id,");
		sql.append("  created_at,");
		sql.append("  updated_at,");
		sql.append("  deleted_at ");
		return sql.toString();
	}

}
