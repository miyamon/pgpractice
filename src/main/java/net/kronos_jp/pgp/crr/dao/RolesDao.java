package net.kronos_jp.pgp.crr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import net.kronos_jp.pgp.crr.entity.Roles;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * {@link Roles}用のDAO。
 */
@Component
public class RolesDao extends AbstractDao {

	/**
	 * 全てのロールを取得します。※論理削除されたデータは含まれない。
	 * 
	 * @return 全てのロール
	 */
	public List<Roles> listAll() {
		return jdbcTemplate
				.query("SELECT id, name, role, created_user_id, updated_user_id, created_at, updated_at, deleted_at "
						+ "FROM roles WHERE deleted_at IS NULL ORDER BY id",
						new RowMapper<Roles>() {
							@Override
							public Roles mapRow(ResultSet rs, int rowNum) throws SQLException {
								Roles result = new Roles();
								result.setId(rs.getInt("id"));
								result.setName(rs.getString("name"));
								result.setRole(rs.getString("role"));
								result.setCreatedUserId(rs.getInt("created_user_id"));
								result.setUpdatedUserId(rs.getInt("updated_user_id"));
								result.setCreatedAt(rs.getTimestamp("created_at"));
								result.setUpdatedAt(rs.getTimestamp("updated_at"));
								return result;
							}
						});

	}
}
