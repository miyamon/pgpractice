package net.kronos_jp.pgp.crr.service;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import net.kronos_jp.pgp.crr.common.DateUtils;
import net.kronos_jp.pgp.crr.dao.RoomsDao;
import net.kronos_jp.pgp.crr.dto.RoomDto;
import net.kronos_jp.pgp.crr.entity.Rooms;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;

@Service
public class RoomService {

	@Autowired
	RoomsDao roomsDao;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * 全ての会議室情報を取得します。※論理削除されたデータは含まれない。
	 *
	 * @return 全ての会議室
	 */
	public List<RoomDto> listAll() {
		return roomsDao.listAll();
	}

	/**
	 * IDから会議室情報を取得します。
	 *
	 * @return IDに紐づく会議室情報
	 */
	public RoomDto selectById(Integer id) {
		return roomsDao.selectById(id);
	}

	/**
	 * 登録処理。 更新者、作成者、更新日付、作成日付を設定します。
	 *
	 * @param rooms
	 * @param principal
	 * @return
	 */
	public int insert(Rooms rooms, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		rooms.setUpdatedAt(time);
		rooms.setCreatedAt(time);
		rooms.setUpdatedUserId(userId);
		rooms.setCreatedUserId(userId);

		return roomsDao.insert(rooms);
	}

	/**
	 * 更新処理。 更新日付で楽観ロックをします。
	 *
	 * @param rooms
	 * @param principal
	 * @return
	 */
	public int update(Rooms rooms, Timestamp lock, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		rooms.setUpdatedAt(time);
		rooms.setUpdatedUserId(userId);

		return roomsDao.update(rooms, lock);
	}

	/**
	 * 	論理削除処理。 更新日付で楽観ロックをします。
	 *
	 * @param rooms
	 * @param principal
	 * @return
	 */
	public int delete(Rooms rooms, Timestamp lock, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		rooms.setUpdatedAt(time);
		rooms.setUpdatedUserId(userId);
		rooms.setDeletedAt(time);

		return roomsDao.update(rooms, lock);
	}

}
