package net.kronos_jp.pgp.crr.service;

import java.security.Principal;
import java.sql.Timestamp;

import net.kronos_jp.pgp.crr.common.DateUtils;
import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.entity.Users;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	UsersDao usersDao;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * 登録処理。 更新者、作成者、更新日付、作成日付を設定します。
	 * 
	 * @param users
	 * @param principal
	 * @return
	 */
	public int insert(Users users, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		users.setUpdatedAt(time);
		users.setCreatedAt(time);
		users.setUpdatedUserId(userId);
		users.setCreatedUserId(userId);

		return usersDao.insert(users);
	}

	/**
	 * 更新処理。 更新日付で楽観ロックをします。
	 * 
	 * @param users
	 * @param principal
	 * @return
	 */
	public int update(Users users, Timestamp lock, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		users.setUpdatedAt(time);
		users.setUpdatedUserId(userId);

		return usersDao.update(users, lock);
	}
	
	/**
	 * 	論理削除処理。 更新日付で楽観ロックをします。
	 * 
	 * @param users
	 * @param principal
	 * @return
	 */
	public int delete(Users users, Timestamp lock, Principal principal) {
		Timestamp time = DateUtils.getDBTimestamp();
		Integer userId = userDetailsHelper.getUserId(principal);
		users.setUpdatedAt(time);
		users.setUpdatedUserId(userId);
		users.setDeletedAt(time);

		return usersDao.update(users, lock);
	}

}
