package net.kronos_jp.pgp.crr.helper;

import java.security.Principal;

import net.kronos_jp.pgp.crr.dto.UserInfoDto;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Springセキュリティーからのユーザー情報取得ヘルパー
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserDetailsHelper {

	/**
	 * ユーザーIDを取得します。
	 * @param principal
	 * @return ユーザーID
	 */
	public Integer getUserId(Principal principal) {
		Authentication authentication = (Authentication) principal;
		UserInfoDto userInfo = (UserInfoDto) authentication.getPrincipal();
		return userInfo.getId();
	}

	/**
	 * ユーザー情報を取得します。
	 * @param principal
	 * @return ユーザー情報
	 */
	public UserInfoDto getUserInfo(Principal principal) {
		Authentication authentication = (Authentication) principal;
		return (UserInfoDto) authentication.getPrincipal();
	}

}
