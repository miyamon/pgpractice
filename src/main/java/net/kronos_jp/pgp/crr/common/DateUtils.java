package net.kronos_jp.pgp.crr.common;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

	public static Date getDBDate(TimeZone timeZone) {
		Calendar now = Calendar.getInstance(timeZone);
		Date date = now.getTime();
		return date;
	}

	public static Date getDBDate() {
		return getDBDate(getTimeZone());
	}

	public static Timestamp getDBTimestamp() {
		return new Timestamp(getDBDate(getTimeZone()).getTime());
	}

	public static TimeZone getTimeZone() {
		return getTimeZone(LocaleContext.instance().getLocale());
	}

	public static TimeZone getTimeZone(Locale locale) {
		return TimeZone.getTimeZone("JST");
	}

	public static SimpleDateFormat getDayOfWeekFormat() {
		return new SimpleDateFormat("E", LocaleContext.instance().getLocale());
	}

	public static int getDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DATE);
	}

	public static int toYearMonty(Integer year, Integer month) {
		return Integer.valueOf(String.format("%d%02d", year, month));
	}
}
