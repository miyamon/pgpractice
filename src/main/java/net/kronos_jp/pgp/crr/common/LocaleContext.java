package net.kronos_jp.pgp.crr.common;

import java.util.Locale;

/**
 * ロケール情報
 * 
 */
public abstract class LocaleContext {
	@SuppressWarnings("rawtypes")
	private static ThreadLocal instance = new ThreadLocal() {
		protected Object initialValue() {
			return null;
		};
	};

	public abstract Locale getLocale();

	public abstract void setLocale(Locale locale);

	@SuppressWarnings("unchecked")
	public static LocaleContext instance() {
		LocaleContext ctx = (LocaleContext) instance.get();
		if (ctx != null) {
			return ctx;
		}
		instance.set(new LocaleContextImpl());
		return (LocaleContext) instance.get();
	}

	static class LocaleContextImpl extends LocaleContext {
		private Locale locale;

		public Locale getLocale() {
			if (locale == null) {
				locale = Locale.getDefault();
			}
			return locale;
		}

		public void setLocale(Locale locale) {
			this.locale = locale;
		}

	}
}
