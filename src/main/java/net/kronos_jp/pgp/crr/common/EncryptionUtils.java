package net.kronos_jp.pgp.crr.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptionUtils {

	public static String toSHA1HexString(String password) {

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("SHA-1インスタンス生成エラー");
		}
		byte[] bytePassword = digest.digest(password.getBytes());
		String hexPassword = EncryptionUtils.toHexString(bytePassword);

		return hexPassword;
	}

	/**
	 * Byte[]を16進文字列に変換する
	 * 
	 * @param arrInput
	 * @return
	 */
	public static String toHexString(byte[] arr) {
		StringBuffer buff = new StringBuffer(arr.length * 2);
		for (int i = 0; i < arr.length; i++) {
			// 0xffでbyteのマイナス値をプラスに変換する
			String b = Integer.toHexString(arr[i] & 0xff);
			if (b.length() == 1) {
				buff.append("0");
			}
			buff.append(b);
		}
		return buff.toString();
	}
}
