package net.kronos_jp.pgp.crr.common;

import java.text.SimpleDateFormat;

public final class Constants {

	public enum DateFormat {
		yyyyMMddHHmm("yyyy/MM/dd HH:mm"), Time("HH:mm");

		private String fromat;

		private DateFormat(String fromat) {
			this.fromat = fromat;
		}

		public SimpleDateFormat getFormat() {
			return new SimpleDateFormat(fromat);
		}

		public String getFormatString() {
			return fromat;
		}
	}

	public enum Roles {
		ADMIN("ROLE_ADMIN"), USER("ROLE_USER");

		private String role;

		private Roles(String role) {
			this.role = role;
		}

		public String getRole() {
			return this.role;
		}
	}
}
