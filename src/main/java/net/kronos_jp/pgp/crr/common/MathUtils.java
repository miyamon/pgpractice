package net.kronos_jp.pgp.crr.common;

import java.math.BigDecimal;

public class MathUtils {

	public static BigDecimal HUNDRED = new BigDecimal(100);

	public static BigDecimal THOUSAND = new BigDecimal(1000);

	public static BigDecimal toPercentage(BigDecimal b1, BigDecimal b2) {

		if (b2 == null) {
			b2 = BigDecimal.ZERO;
		}
		if (b1 == null) {
			b1 = BigDecimal.ZERO;
		}

		if (b2.compareTo(BigDecimal.ZERO) == 0) {
			return b2.setScale(1);
		}
		return b1.divide(b2, 3, BigDecimal.ROUND_HALF_UP).multiply(HUNDRED).setScale(1, BigDecimal.ROUND_UNNECESSARY);
	}

	public static BigDecimal add(BigDecimal b1, BigDecimal b2) {
		return b2 == null ? b1 : b1.add(b2);
	}

	public static BigDecimal toDefaultZero(BigDecimal b) {
		return b == null ? BigDecimal.ZERO : b;
	}

	public static BigDecimal toK(BigDecimal b) {
		return b == null ? BigDecimal.ZERO : b.divide(THOUSAND, 0, BigDecimal.ROUND_HALF_UP);
	}
}
