package net.kronos_jp.pgp.crr.web;

import java.security.Principal;

import javax.validation.GroupSequence;

import net.kronos_jp.pgp.crr.common.EncryptionUtils;
import net.kronos_jp.pgp.crr.dao.RolesDao;
import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.entity.Users;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;
import net.kronos_jp.pgp.crr.service.UserService;
import net.kronos_jp.pgp.crr.validator.EqualsValidator.EqualsValidationDto;
import net.kronos_jp.pgp.crr.validator.annotation.AccountDuplicat;
import net.kronos_jp.pgp.crr.validator.annotation.AlphaNumeral;
import net.kronos_jp.pgp.crr.validator.annotation.AlphaNumeralSymbol;
import net.kronos_jp.pgp.crr.validator.annotation.Equals;
import net.kronos_jp.pgp.crr.validator.annotation.Length;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Transactional
public class UserRegisterController {

	@Value("${title.userRegister}")
	String pageTitle;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	UsersDao usersDao;

	@Autowired
	RolesDao rolesDao;

	@Autowired
	UserService userService;

	/**
	 * 初期表示
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userRegister.html", method = RequestMethod.GET)
	public String initialize(Model model, RegistrationUserModel registrationUserModel, Principal principal) {

		model.addAttribute(registrationUserModel);

		model.addAttribute("roles", rolesDao.listAll());
		model.addAttribute("pageTitle", pageTitle);
		return "userRegister";
	}

	/**
	 * 登録処理
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userRegister.html", method = RequestMethod.POST)
	public String registrationUser(Model model,
			@Validated(ValidationSequenceInterrelationPassoword.class) RegistrationUserModel registrationUserModel,
			BindingResult result, Principal principal) {

		// 入力チェックでerrorが発生した場合は、同じ画面に
		if (result.hasErrors()) {
			model.addAttribute("roles", rolesDao.listAll());
			model.addAttribute("pageTitle", pageTitle);
			return "userRegister";
		}

		Users users = new Users();
		users.setAccount(registrationUserModel.getAccount());
		users.setName(registrationUserModel.getUserName());
		users.setPassword(EncryptionUtils.toSHA1HexString(registrationUserModel.getPassword()));
		users.setRoleId(registrationUserModel.getRole());

		// サービスへ処理を移譲します
		userService.insert(users, principal);

		return "redirect:userList.html";
	}

	public static class RegistrationUserModel {

		private Integer id;

		@AccountDuplicat(groups = { ValidathiontGroup.class })
		@AlphaNumeral(groups = { ValidathiontGroup.class })
		@NotBlank(groups = { ValidathiontGroup.class })
		@Length(max = 30, message = "{org.hibernate.validator.constraints.Length.max.message}", groups = { ValidathiontGroup.class })
		private String account;

		@NotBlank(groups = { ValidathiontGroup.class })
		@Length(max = 30, message = "{org.hibernate.validator.constraints.Length.max.message}", groups = { ValidathiontGroup.class })
		private String userName;

		@NotBlank(groups = { ValidathiontGroup.class })
		@AlphaNumeralSymbol(groups = { ValidathiontGroup.class })
		@Length(min = 8, max = 40, message = "{org.hibernate.validator.constraints.Length.message}", groups = { ValidationPassowordGroup.class })
		private String password;

		private String confirmationPassword;

		private Integer role;

		@Equals(groups = { ValidathiontGroup.class })
		public EqualsValidationDto getInterrelationPassoword() {
			return new EqualsValidationDto(password, confirmationPassword);
		}

		public Integer getRole() {
			return role;
		}

		public void setRole(Integer role) {
			this.role = role;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getConfirmationPassword() {
			return confirmationPassword;
		}

		public void setConfirmationPassword(String confirmationPassword) {
			this.confirmationPassword = confirmationPassword;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

	}

	@GroupSequence({ ValidathiontGroup.class, ValidationPassowordGroup.class })
	public static interface ValidationSequenceInterrelationPassoword {
	}

	public static interface ValidathiontGroup {
	}

	public static interface ValidationPassowordGroup {
	}
}
