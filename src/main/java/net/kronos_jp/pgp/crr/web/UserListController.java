package net.kronos_jp.pgp.crr.web;

import java.security.Principal;
import java.util.List;

import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.dto.UserDto;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Transactional
public class UserListController {

	private static final Logger logger = LoggerFactory.getLogger(UserListController.class);

	@Value("${title.userList}")
	String pageTitle;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	UsersDao usersDao;

	/**
	 * 初期表示
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userList.html", method = RequestMethod.GET)
	public String initialize(Model model, Principal principal) {

		List<UserDto> userList = usersDao.listAll();
		if (logger.isDebugEnabled()) {
			logger.debug(userList.toString());
		}

		model.addAttribute("userList", userList);
		model.addAttribute("pageTitle", pageTitle);
		return "userList";
	}

}
