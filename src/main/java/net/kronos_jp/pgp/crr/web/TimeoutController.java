package net.kronos_jp.pgp.crr.web;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TimeoutController {

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@RequestMapping(value = "/timeout.html", method = RequestMethod.GET)
	public String timeout(HttpServletResponse response) {
		response.addHeader("session-status", "timeout");
		return "timeout";
	}
}
