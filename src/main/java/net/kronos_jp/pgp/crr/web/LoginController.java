package net.kronos_jp.pgp.crr.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * ログイン関連のController
 *
 */
@Controller
@Transactional
public class LoginController {

	@Value("${title.index}")
	String pageTitle;

	/**
	 * /でアクセスされた場合にindex.htmlにforwardさせる。
	 * @param model
	 * @param redirectAttributes
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public String showLoginPage(Model model, RedirectAttributes redirectAttributes, Principal principal) {

		model.addAttribute("pageTitle", pageTitle);
		return "index";
	}

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * ログイン失敗時のコントロールをする。Messageのだし分け。
	 * 
	 * @param fail
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/login/fail", method = RequestMethod.GET)
	public String fail(Fail fail, RedirectAttributes attributes) {
		String code = fail.getCode();
		List<String> errosrs = new ArrayList<String>();
		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);

		if ("both".equals(code)) {
			errosrs.add(message.getMessage("net.kronos_jp.pgp.crr.error.user"));
			errosrs.add(message.getMessage("net.kronos_jp.pgp.crr.error.password"));
		} else if ("user".equals(code)) {
			errosrs.add(message.getMessage("net.kronos_jp.pgp.crr.error.user"));
		} else if ("password".equals(code)) {
			errosrs.add(message.getMessage("net.kronos_jp.pgp.crr.error.password"));
		} else if ("badCredentials".equals(code)) {
			errosrs.add(message.getMessage("net.kronos_jp.pgp.crr.error.login"));
		}
		attributes.addFlashAttribute("errors", errosrs);

		return "redirect:/";
	}

	public static class Fail {
		private String code;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		private String userId;
		private String password;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}

}
