package net.kronos_jp.pgp.crr.web;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.kronos_jp.pgp.crr.dto.RoomDto;
import net.kronos_jp.pgp.crr.service.RoomService;

@Controller
@Transactional
public class RoomListController {

	private static final Logger logger = LoggerFactory.getLogger(RoomListController.class);

	@Value("${title.roomList}")
	String pageTitle;

	@Autowired
	RoomService roomService;

	/**
	 * 初期表示
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/roomList.html", method = RequestMethod.GET)
	public String initialize(Model model, Principal principal) {

		List<RoomDto> roomList = roomService.listAll();
		if (logger.isDebugEnabled()) {
			logger.debug(roomList.toString());
		}

		model.addAttribute("roomList", roomList);
		model.addAttribute("pageTitle", pageTitle);
		return "roomList";
	}

}
