package net.kronos_jp.pgp.crr.web;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.kronos_jp.pgp.crr.common.Constants.Roles;
import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.dto.UserDto;
import net.kronos_jp.pgp.crr.entity.Rooms;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;
import net.kronos_jp.pgp.crr.service.RoomService;
import net.kronos_jp.pgp.crr.validator.annotation.Length;

@Controller
@Transactional
public class RoomEditController {

	@Value("${title.roomEdit}")
	String pageTitle;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	UsersDao usersDao;

	@Autowired
	RoomService roomService;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * 初期表示
	 *
	 * @param model
	 * @param editRoomModel
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/roomEdit.html", method = RequestMethod.GET)
	public String initialize(Model model, EditRoomModel editRoomModel, Principal principal) {

		UserDto userDto = usersDao.selectById(userDetailsHelper.getUserId(principal));
		// ユーザの権限が管理者でない場合は予約一覧画面に遷移する。
		if (!Roles.ADMIN.getRole().equals(userDto.getRole())) {
			return "redirect:reservationList.html";
		}

		Integer id = editRoomModel.getId();
		// IDがnullの場合は、新規作成。nullでない場合は編集。
		if (id != null) {
			// 編集の場合、IDを元に会議室情報を検索して情報を設定する。
			dozerBeanMapper.map(roomService.selectById(editRoomModel.getId()), editRoomModel);
		}
		model.addAttribute(editRoomModel);
		model.addAttribute("pageTitle", pageTitle);
		return "roomEdit";
	}

	/**
	 * 削除処理
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/roomEdit.html", method = RequestMethod.POST, params = { "delete" })
	public String deleteUser(RedirectAttributes redirectAttributes, Model model, EditRoomModel editRoomModel,
			Principal principal) {

		Timestamp timestamp = editRoomModel.getUpdatedAt();
		Rooms rooms = roomService.selectById(editRoomModel.getId());

		// サービスへ処理を移譲します（削除処理）
		int count = roomService.delete(rooms, timestamp, principal);
		// 1より小さい場合楽観ロックで更新出来ないパターンのためエラーとする
		if (count < 1) {
			List<String> errors = new ArrayList<String>();
			MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
			errors.add(message.getMessage("net.kronos_jp.pgp.crr.error.optimisticLock"));
			redirectAttributes.addFlashAttribute("errors", errors);
		}

		return "redirect:roomList.html";
	}

	/**
	 * 更新処理
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/roomEdit.html", method = RequestMethod.POST, params = { "save" })
	public String editUser(RedirectAttributes redirectAttributes, Model model,
			@Validated(ValidationGroup.class) EditRoomModel editRoomModel, BindingResult result,
			Principal principal) {

		// 入力チェックでerrorが発生した場合は、同じ画面に遷移する。
		if (result.hasErrors()) {
			model.addAttribute("pageTitle", pageTitle);
			return "roomEdit";
		}

		Integer id = editRoomModel.getId();
		if (id != null) {
			// IDが存在する場合は編集処理を実施する。
			Timestamp timestamp = editRoomModel.getUpdatedAt();
			Rooms rooms = roomService.selectById(editRoomModel.getId());
			rooms.setName(editRoomModel.getName());
			// サービスへ処理を移譲します
			int count = roomService.update(rooms, timestamp, principal);
			// 1より小さい場合楽観ロックで更新出来ないパターンのためエラーとする
			if (count < 1) {
				List<String> errors = new ArrayList<String>();
				MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
				errors.add(message.getMessage("net.kronos_jp.pgp.crr.error.optimisticLock"));
				redirectAttributes.addFlashAttribute("errors", errors);
			}
		} else {
			// IDが存在しない場合は新規登録処理を実施する。
			Rooms rooms = dozerBeanMapper.map(editRoomModel, Rooms.class);
			// サービスへ処理を移譲します
			roomService.insert(rooms, principal);
		}

		return "redirect:roomList.html";
	}

	public static class EditRoomModel {

		private Integer id;

		@NotBlank(groups = { ValidationGroup.class })
		@Length(max = 30, message = "{org.hibernate.validator.constraints.Length.max.message}", groups = { ValidationGroup.class })
		private String name;

		private Timestamp updatedAt;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Timestamp getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Timestamp updatedAt) {
			this.updatedAt = updatedAt;
		}
	}

	public static interface ValidationGroup {
	}
}
