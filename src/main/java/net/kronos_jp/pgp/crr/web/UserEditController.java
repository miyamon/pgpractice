package net.kronos_jp.pgp.crr.web;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.GroupSequence;

import net.kronos_jp.pgp.crr.common.EncryptionUtils;
import net.kronos_jp.pgp.crr.dao.RolesDao;
import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.entity.Users;
import net.kronos_jp.pgp.crr.helper.UserDetailsHelper;
import net.kronos_jp.pgp.crr.service.UserService;
import net.kronos_jp.pgp.crr.validator.EqualsValidator.EqualsValidationDto;
import net.kronos_jp.pgp.crr.validator.annotation.AlphaNumeralSymbol;
import net.kronos_jp.pgp.crr.validator.annotation.Equals;
import net.kronos_jp.pgp.crr.validator.annotation.Length;
import net.kronos_jp.pgp.crr.validator.annotation.UserExists;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
public class UserEditController {

	@Value("${title.userEdit}")
	String pageTitle;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	UsersDao usersDao;

	@Autowired
	RolesDao rolesDao;

	@Autowired
	UserService userService;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * 初期表示
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userEdit.html", method = RequestMethod.GET)
	public String initialize(Model model, @Validated(ValidathiontInitializeGroup.class) EditUserModel editUserModel,
			BindingResult result, Principal principal, RedirectAttributes redirectAttributes) {

		// ユーザーが存在しない場合は一覧に戻す
		if (result.hasErrors()) {
			return "redirect:userList.html";
		}

		Users users = usersDao.selectById(editUserModel.getId());

		editUserModel.setAccount(users.getAccount());
		editUserModel.setRole(users.getRoleId());
		editUserModel.setUserName(users.getName());
		editUserModel.setUpdatedAt(users.getUpdatedAt());

		model.addAttribute(editUserModel);

		model.addAttribute("roles", rolesDao.listAll());
		model.addAttribute("pageTitle", pageTitle);
		return "userEdit";
	}

	/**
	 * 削除処理
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userEdit.html", method = RequestMethod.POST, params = { "delete" })
	public String deleteUser(RedirectAttributes redirectAttributes, Model model, EditUserModel editUserModel, Principal principal) {


		Timestamp timestamp = editUserModel.getUpdatedAt();
		Users users = usersDao.selectById(editUserModel.getId());

		// サービスへ処理を移譲します
		int count = userService.delete(users, timestamp, principal);
		// 1より小さい場合楽観ロックで更新出来ないパターンのためエラーとする
		if (count < 1) {
			List<String> errors = new ArrayList<String>();
			MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
			errors.add(message.getMessage("net.kronos_jp.pgp.crr.error.optimisticLock"));
			redirectAttributes.addFlashAttribute("errors", errors);
		}

		return "redirect:userList.html";
	}

	/**
	 * 更新処理
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/manage/userEdit.html", method = RequestMethod.POST, params = { "save" })
	public String editUser(RedirectAttributes redirectAttributes, Model model,
			@Validated(ValidationSequenceInterrelationPassoword.class) EditUserModel editUserModel,
			BindingResult result, Principal principal) {

		// 入力チェックでerrorが発生した場合は、同じ画面に
		if (result.hasErrors()) {
			model.addAttribute("roles", rolesDao.listAll());
			model.addAttribute("pageTitle", pageTitle);
			return "userEdit";
		}

		Timestamp timestamp = editUserModel.getUpdatedAt();

		Users users = usersDao.selectById(editUserModel.getId());
		users.setAccount(editUserModel.getAccount());
		users.setName(editUserModel.getUserName());
		// パスワードは入力がないと変更はしない
		if (StringUtils.isNotEmpty(editUserModel.getPassword())) {
			users.setPassword(EncryptionUtils.toSHA1HexString(editUserModel.getPassword()));
		}
		users.setRoleId(editUserModel.getRole());

		// サービスへ処理を移譲します
		int count = userService.update(users, timestamp, principal);
		// 1より小さい場合楽観ロックで更新出来ないパターンのためエラーとする
		if (count < 1) {
			List<String> errors = new ArrayList<String>();
			MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
			errors.add(message.getMessage("net.kronos_jp.pgp.crr.error.optimisticLock"));
			redirectAttributes.addFlashAttribute("errors", errors);
		}

		return "redirect:userList.html";
	}

	public static class EditUserModel {

		@UserExists(groups = { ValidathiontInitializeGroup.class })
		private Integer id;

		private String account;

		@NotBlank(groups = { ValidathiontGroup.class })
		@Length(max = 30, message = "{org.hibernate.validator.constraints.Length.max.message}", groups = { ValidathiontGroup.class })
		private String userName;

		// パスワードは入力がないと変更はしないので必須チェックはつけない
		@AlphaNumeralSymbol(groups = { ValidathiontGroup.class })
		@Length(min = 8, max = 40, message = "{org.hibernate.validator.constraints.Length.message}", groups = { ValidationPassowordGroup.class })
		private String password;

		private String confirmationPassword;

		private Integer role;

		private Timestamp updatedAt;

		@Equals(groups = { ValidathiontGroup.class })
		public EqualsValidationDto getInterrelationPassoword() {
			return new EqualsValidationDto(password, confirmationPassword);
		}

		public Integer getRole() {
			return role;
		}

		public void setRole(Integer role) {
			this.role = role;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getConfirmationPassword() {
			return confirmationPassword;
		}

		public void setConfirmationPassword(String confirmationPassword) {
			this.confirmationPassword = confirmationPassword;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Timestamp getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Timestamp updatedAt) {
			this.updatedAt = updatedAt;
		}

	}

	@GroupSequence({ ValidathiontGroup.class, ValidationPassowordGroup.class })
	public static interface ValidationSequenceInterrelationPassoword {
	}

	public static interface ValidathiontInitializeGroup {
	}

	public static interface ValidathiontGroup {
	}

	public static interface ValidationPassowordGroup {
	}
}
