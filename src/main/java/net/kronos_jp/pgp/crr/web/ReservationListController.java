package net.kronos_jp.pgp.crr.web;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 予約一覧画面コントローラー
 */
@Controller
@Transactional
public class ReservationListController {

	@Value("${title.reservationList}")
	String pageTitle;

	/**
	 * 初期表示
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/reservationList.html", method = RequestMethod.GET)
	public String initialize(Model model, Principal principal) {

		model.addAttribute("pageTitle", pageTitle);
		return "reservationList";
	}
}
