package net.kronos_jp.pgp.crr.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.kronos_jp.pgp.crr.aop.WebErrorHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.savedrequest.RequestCache;

/**
 * ログイン時にエラーが発生したときの例外ハンドリング。ログインしていない状態では{@link WebErrorHandler}が有効にならないためここで対応。
 *
 */
public class LoginErrorFilter extends ExceptionTranslationFilter {

	/**
	 * @see ExceptionTranslationFilter#ExceptionTranslationFilter()
	 * @deprecated Use constructor injection
	 */
	@Deprecated
	public LoginErrorFilter() {
		super();
	}

	public LoginErrorFilter(AuthenticationEntryPoint authenticationEntryPoint) {
		super(authenticationEntryPoint);
	}

	public LoginErrorFilter(AuthenticationEntryPoint authenticationEntryPoint, RequestCache requestCache) {
		super(authenticationEntryPoint, requestCache);
	}

	@Autowired
	WebErrorHandler webErrorHandler;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		try {
			super.doFilter(request, response, chain);
		} catch (Exception e) {
			webErrorHandler.resolveException(req, res, null, e);
			RequestDispatcher disp = req.getRequestDispatcher("/500.jsp");
			disp.forward(req, res);
		}
	}

}
