package net.kronos_jp.pgp.crr.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

/**
 * Springへアクセスした際の認証エラーをハンドリングするクラス。
 * 
 * セッションは有効の場合、かつ認証がされていない状態でのみ呼ばれる。
 * セッションが無効の場合は、session-managementのinvalid-session-urlが有効となり、本クラスは呼ばれない。
 * 
 * AJAXのアクセスで認証エラーとなる場合はタイムアウト後のアクセスであると判断できるため
 * 本クラスで{@link HttpServletResponse#SC_UNAUTHORIZED}を返却。
 * 
 * 通常のアクセスでの認証エラーは本クラスでは処理せず、親クラスの処理を呼び出す。
 */
public class AjaxAuthenticationProcessingEntryPoint extends LoginUrlAuthenticationEntryPoint {

	public AjaxAuthenticationProcessingEntryPoint(String loginUrl) {
		super(loginUrl);
	}
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		String requestType = request.getHeader("x-requested-with");
		if (requestType != null && requestType.equals("XMLHttpRequest")) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		super.commence(request, response, authException);
	}
}
