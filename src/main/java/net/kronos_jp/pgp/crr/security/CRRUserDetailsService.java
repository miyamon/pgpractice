package net.kronos_jp.pgp.crr.security;

import java.util.ArrayList;
import java.util.List;

import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.dto.UserDto;
import net.kronos_jp.pgp.crr.dto.UserInfoDto;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CRRUserDetailsService implements UserDetailsService {

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Autowired
	UsersDao usersDao;

	@Override
	@Transactional(readOnly = true)
	public UserInfoDto loadUserByUsername(String username) throws UsernameNotFoundException {

		UserDto user = usersDao.selectByAccount(username);
		if (user == null) {
			return null;
		}

		UserInfoDto userInfo = dozerBeanMapper.map(user, UserInfoDto.class);
		userInfo.setLogin(true);
		userInfo.setEnabled(true);

		List<GrantedAuthority> collection = new ArrayList<>();
		collection.add(new SimpleGrantedAuthority(user.getRole()));
		userInfo.setAuthorities(collection);
		return userInfo;
	}
}
