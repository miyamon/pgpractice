package net.kronos_jp.pgp.crr.security;

import org.springframework.security.core.AuthenticationException;

/**
 * パスワード、ユーザーID未入力エラー
 *
 */
public class BothNotEnterException extends AuthenticationException {

	private static final long serialVersionUID = 6348734582122226607L;

	public BothNotEnterException(String msg) {
		super(msg);
	}

}
