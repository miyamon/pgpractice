package net.kronos_jp.pgp.crr.security;

import org.springframework.security.core.AuthenticationException;

/**
 * パスワード未入力例外
 *
 */
public class PasswordNotEnterException extends AuthenticationException {

	private static final long serialVersionUID = 6348734582122226607L;

	public PasswordNotEnterException(String msg) {
		super(msg);
	}

}
