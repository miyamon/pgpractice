package net.kronos_jp.pgp.crr.security;

import org.springframework.security.core.AuthenticationException;

/**
 * ユーザー名が見つからないエラー
 */
public class UserNameNotEnterException extends AuthenticationException {

	private static final long serialVersionUID = 6348734582122226607L;

	public UserNameNotEnterException(String msg) {
		super(msg);
	}

}
