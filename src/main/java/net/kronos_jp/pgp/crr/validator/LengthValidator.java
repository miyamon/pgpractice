package net.kronos_jp.pgp.crr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.validator.annotation.Length;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

/**
 * {@link org.hibernate.validator.constraints.Length}がEmptyを許容しないのでカスタマイズ
 * 
 * @see org.hibernate.validator.internal.constraintvalidators.LengthValidator
 *
 */
public class LengthValidator implements ConstraintValidator<Length, CharSequence> {

	private static final Log log = LoggerFactory.make();

	private int min;
	private int max;

	public void initialize(Length parameters) {
		min = parameters.min();
		max = parameters.max();
		validateParameters();
	}

	public boolean isValid(CharSequence value, ConstraintValidatorContext constraintValidatorContext) {
		if (StringUtils.isEmpty(value)) {
			return true;
		}
		int length = value.length();
		return length >= min && length <= max;
	}

	private void validateParameters() {
		if (min < 0) {
			throw log.getMinCannotBeNegativeException();
		}
		if (max < 0) {
			throw log.getMaxCannotBeNegativeException();
		}
		if (max < min) {
			throw log.getLengthCannotBeNegativeException();
		}
	}
}
