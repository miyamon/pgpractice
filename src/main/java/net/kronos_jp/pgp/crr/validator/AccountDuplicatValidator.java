package net.kronos_jp.pgp.crr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.validator.annotation.AccountDuplicat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Account用バリデータ
 * DBに接続してAccountの存在チェックをします。
 *
 */
@Component
public class AccountDuplicatValidator implements ConstraintValidator<AccountDuplicat, String> {
	
	@Autowired
	UsersDao usersDao;
	
	@Override
	public void initialize(AccountDuplicat ans) {
	}

	@Override
	public boolean isValid(String param, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(param)) {
			return true;
		}
		return usersDao.countAccount(param) < 1;
	}
}
