package net.kronos_jp.pgp.crr.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.validator.EqualsValidator.EqualsValidationDto;
import net.kronos_jp.pgp.crr.validator.annotation.Equals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Account用バリデータ DBに接続してAccountの存在チェックをします。
 *
 */
@Component
public class EqualsValidator implements ConstraintValidator<Equals, EqualsValidationDto> {

	@Autowired
	UsersDao usersDao;

	@Override
	public void initialize(Equals ans) {
	}

	@Override
	public boolean isValid(EqualsValidationDto param, ConstraintValidatorContext context) {

		if (param == null) {
			return true;
		}

		return Objects.equals(param.o1, param.o2);
	}

	public static class EqualsValidationDto {

		public EqualsValidationDto(Object o1, Object o2) {
			super();
			this.o1 = o1;
			this.o2 = o2;
		}

		public Object getO1() {
			return o1;
		}

		public void setO1(Object o1) {
			this.o1 = o1;
		}

		public Object getO2() {
			return o2;
		}

		public void setO2(Object o2) {
			this.o2 = o2;
		}

		private Object o1;
		private Object o2;

	}
}
