package net.kronos_jp.pgp.crr.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.validator.annotation.AlphaNumeral;

import org.apache.commons.lang3.StringUtils;

/**
 * Account用バリデータ
 *
 */
public class AlphaNumeralValidator implements ConstraintValidator<AlphaNumeral, String> {

	private static final String ALPHABET_NUMERAL= "[a-zA-Z0-9]*";
	
	@Override
	public void initialize(AlphaNumeral ans) {
	}

	@Override
	public boolean isValid(String param, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(param)) {
			return true;
		}
		return Pattern.matches(ALPHABET_NUMERAL, param);
	}
}
