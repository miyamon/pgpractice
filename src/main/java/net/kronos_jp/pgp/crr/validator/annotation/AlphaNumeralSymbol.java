package net.kronos_jp.pgp.crr.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import net.kronos_jp.pgp.crr.validator.AlphaNumeralSymbolValidator;

@Documented
@Constraint(validatedBy = AlphaNumeralSymbolValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface AlphaNumeralSymbol {
	String charset() default "UTF-8";

	String message() default "{net.kronos_jp.pgp.crr.validator.AlphaNumeralSymbol.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
	
	/**
	 * Defines several {@code @ByteLength} annotations on the same element.
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
	@Retention(RUNTIME)
	@Documented
	public @interface List {
		AlphaNumeralSymbol[] value();
	}
}
