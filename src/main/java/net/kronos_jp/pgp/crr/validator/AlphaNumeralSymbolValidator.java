package net.kronos_jp.pgp.crr.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.validator.annotation.AlphaNumeralSymbol;

import org.apache.commons.lang3.StringUtils;

/**
 * 半角英数記号のみ含まれているかの判定用バリデータ。
 *
 */
public class AlphaNumeralSymbolValidator implements ConstraintValidator<AlphaNumeralSymbol, String> {

	private static final String ALPHABET_NUMERAL_SIMBOL = "[a-zA-Z0-9\\Q!\"#$%&'()=~|-^\\@`[;:],./{*+}<>?_\\E]*";

	@Override
	public void initialize(AlphaNumeralSymbol ans) {
	}

	@Override
	public boolean isValid(String param, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(param)) {
			return true;
		}
		return Pattern.matches(ALPHABET_NUMERAL_SIMBOL, param);
	}
}
