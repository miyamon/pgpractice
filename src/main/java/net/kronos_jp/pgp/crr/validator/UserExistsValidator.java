package net.kronos_jp.pgp.crr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import net.kronos_jp.pgp.crr.dao.UsersDao;
import net.kronos_jp.pgp.crr.validator.annotation.UserExists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ユーザー存在チェックバリデータ DBに接続してユーザーの存在チェックをします。
 *
 */
@Component
public class UserExistsValidator implements ConstraintValidator<UserExists, Integer> {

	@Autowired
	UsersDao usersDao;

	@Override
	public void initialize(UserExists ans) {
	}

	@Override
	public boolean isValid(Integer id, ConstraintValidatorContext context) {

		if (id == null) {
			return false;
		}

		return usersDao.selectById(id) != null;

	}
}
