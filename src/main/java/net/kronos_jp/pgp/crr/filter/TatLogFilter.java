package net.kronos_jp.pgp.crr.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * サーブレット呼び出し時のTATをログ出力するフィルタ。
 */
@WebFilter(filterName = "tatLogFilter", urlPatterns = "/*")
public class TatLogFilter implements Filter {

	public static final Logger logger = Logger.getLogger("tat");

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// NOP
	}

	@Override
	public void destroy() {
		// NOP
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
			chain.doFilter(request, response);
			return;
		}

		long start = System.currentTimeMillis();
		try {
			chain.doFilter(request, response);
		} finally {
			if (logger.isInfoEnabled()) {
				long end = System.currentTimeMillis();
				String url = ((HttpServletRequest) request).getRequestURI();
				logger.info("RequestUrl [" + url + "] Start [" + start + "]" + " End [" + end + "] Time ["
						+ (end - start) + "]");
			}
		}
	}
}
