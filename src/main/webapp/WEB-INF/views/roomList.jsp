<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${pageTitle}</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/whhg.css" />
</head>
<body>
    <form:form action="${pageContext.request.contextPath}/roomEdit.html" method="GET" modelAttribute="editEditModel" name="editForm">
        <c:import url="./header.jsp">
            <c:param name="page" value="room" />
        </c:import>
        <div class="content">
            <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                <div class="button-area" style="margin-top: 20px;">
                    <a class="button" href="<c:url value = '/roomEdit.html' />">新規登録</a>
                </div>
            </sec:authorize>
            <div class="list-area" style="margin-top: 20px;">
                <c:if test="${not empty roomList}">
                    <table id="usre-table" class="list" style="width: 100%">
                        <tbody>
                            <c:forEach var="room" items="${roomList}">
                                <tr>
                                    <td style="width: 30px;">
                                        <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                                            <a style="color: #cb8fc2; text-decoration: underline;" id="<c:out value='${room.id}'/>" href="<c:url value = '/roomEdit.html?id=${room.id}' />"><i style="font-size: 1.2em" class="icon-pencil"></i></a>
                                        </sec:authorize>
                                    </td>
                                    <td style="width: 150px;"><c:out value="${room.name}" /></td>
                                    <td style="width: auto;"></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:if>
            </div>
        </div>
    </form:form>
</body>

</html>