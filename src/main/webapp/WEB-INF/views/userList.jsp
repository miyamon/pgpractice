<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${pageTitle}</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/whhg.css" />
</head>
<body>
    <form:form action="${pageContext.request.contextPath}/manage/userRegister.html" method="GET" modelAttribute="editUserModel" name="editForm">
        <c:import url="./header.jsp">
            <c:param name="page" value="user" />
        </c:import>
        <div class="content">
            <div class="button-area" style="margin-top: 20px;">
                <a class="button" href="<c:url value = '/manage/userRegister.html' />">新規登録</a>
            </div>
            <div class="list-area" style="margin-top: 20px;">

                <c:if test="${not empty userList}">
                    <table id="usre-table" class="list" style="width: 100%">
                        <tbody>
                            <c:forEach var="user" items="${userList}">
                                <tr>
                                    <td style="width: 30px;"><a style="color: #cb8fc2; text-decoration: underline;" id="<c:out value='${user.id}'/>" href="<c:url value = '/manage/userEdit.html?id=${user.id}' />"><i style="font-size: 1.2em" class="icon-pencil"></i></a></td>
                                    <td style="width: 150px;"><c:out value="${user.account}" /></td>
                                    <td style="width: 150px;"><c:out value="${user.name}" /></td>
                                    <td style="width: 150px;"><c:out value="${user.roleName}" /></td>
                                    <td style="width: auto;"></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:if>
            </div>
        </div>
    </form:form>
</body>

</html>