<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>${pageTitle}</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />

</head>
<body class="pentaho-page-background" style="margin:0px;">

	<div id="container">
            <div style="width: 360px; padding: 30px 30px 30px 30px; margin: 40px 0 0 100px; position: relative;">
            <label style="font-size: 3.0em; font-family: &amp;amp; quot;">会議室予約</label><label style="color: #FFF; font-size: 1.5em; font-family: &amp;amp; quot;"></label><br/>
    
			<form:form action="${pageContext.request.contextPath}/login" method="POST" style="margin-top:30px;">
                <c:if test="${not empty errors}">
                    <c:forEach var="error" items="${errors}">
                        <p class="error_message">${error}</p>
                    </c:forEach>
                </c:if>
                <form:errors path="error" />
				<label id="forUserid" style="font-size: 1.0em; font-family: &amp;amp; quot; line-height: 20px;">User ID:</label><br/>
				<input type="text" name="userId" id="userId" style="border: 1px solid #333; padding: 4px; width: 190px; height: 17px;" /><br/>
				<label id="forPassword" style="font-size: 1.0em; font-family: &amp;amp; quot; line-height:20px;">Password:</label><br/>
				<input type="password" name="password" id="password" style="border: 1px solid #333; padding: 4px; width: 190px; height: 17px;" /><br/>
				<input type="submit" id="login_0" value="Login" class="pentaho-button" style="float: right; clear: both;"/>
			</form:form>
		</div>
	</div>
</body>
</html>