<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>


<div class="header">
    <ul class="header-left">
        <li class="title" style="padding-right: 20px"><span>${pageTitle}</span></li>
        <li><a class="menu <c:if test='${param.page == "reserve"}'>selected</c:if>" href="<c:url value = '/reservationList.html' />">予約</a></li>
        <li><a class="menu <c:if test='${param.page == "room"}'>selected</c:if>" href="<c:url value = '/roomList.html' />">会議室管理</a></li>
        <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
            <li><a class="menu <c:if test='${param.page == "user"}'>selected</c:if>" href="<c:url value = '/manage/userList.html' />">ユーザー管理</a></li>
        </sec:authorize>
        <li style="float: right;"></li>
        <li><span><sec:authentication property="principal.name" /></span></li>
        <li><span class="button-container"><a class="button" href="<c:url value = '/logout' />">Logout</a></span></li>
    </ul>
</div>
<div class="error">
    <c:if test="${not empty errors}">
        <c:forEach var="error" items="${errors}">
            <p class="error_message">${error}</p>
        </c:forEach>
    </c:if>
    <form:errors path="*" element="p" cssClass="error_message" />
</div>