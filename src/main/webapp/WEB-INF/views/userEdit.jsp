<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${pageTitle}</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />
</head>
<body>
    <form:form action="${pageContext.request.contextPath}/manage/userEdit.html" method="POST" modelAttribute="editUserModel">
        <c:import url="./header.jsp">
            <c:param name="page" value="user" />
        </c:import>

        <div class="content">
            <div class="input-area" style="margin-top: 20px;">
                <div>
                    <table class="input">
                        <tbody>
                            <tr>
                                <th>アカウント</th>
                                <td><c:out value="${editUserModel.account}" /> <form:hidden path="account" /></td>
                            </tr>
                            <tr>
                                <th>パスワード</th>
                                <td><form:input path="password" type="password" maxlength="40" size="30" /></td>
                            </tr>
                            <tr>
                                <th>パスワード（確認用）</th>
                                <td><form:input path="confirmationPassword" type="password" maxlength="40" size="30" /></td>
                            </tr>
                            <tr>
                                <th>ユーザー名</th>
                                <td><form:input path="userName" type="text" maxlength="30" size="30" /></td>
                            </tr>
                            <tr>
                                <th>権限</th>
                                <td><form:select path="role" items="${roles}" itemLabel="name" itemValue="id">
                                    </form:select></td>
                            </tr>
                        </tbody>
                    </table>
                    <form:hidden path="updatedAt" />
                    <form:hidden path="id" />
                </div>
            </div>
            <div class="button-area" style="margin-top: 20px;">
                <a class="button" href="<c:url value = '/manage/userList.html' />">戻る</a> <input class="button" type="submit" value="保存" name="save" /> <input class="button" type="submit" value="削除" name="delete"/>
            </div>

        </div>
    </form:form>
</body>
</html>