<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${pageTitle}</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />
</head>
<body>
    <form:form action="${pageContext.request.contextPath}/roomEdit.html" method="POST" modelAttribute="editRoomModel">
        <c:import url="./header.jsp">
            <c:param name="page" value="room" />
        </c:import>

        <div class="content">
            <div class="input-area" style="margin-top: 20px;">
                <div>
                    <table class="input">
                        <tbody>
                            <tr>
                                <th>会議室名</th>
                                <td><form:input path="name" type="text" maxlength="30" size="30" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <c:if test="${not empty editRoomModel.id}">
                        <form:hidden path="updatedAt" />
                        <form:hidden path="id" />
                    </c:if>
                </div>
            </div>
            <div class="button-area" style="margin-top: 20px;">
                <a class="button" href="<c:url value = '/roomList.html' />">戻る</a>
                <input class="button" type="submit" value="保存" name="save" />
                <c:if test="${not empty editRoomModel.id}"><input class="button" type="submit" value="削除" name="delete"/></c:if>
            </div>
        </div>
    </form:form>
</body>
</html>