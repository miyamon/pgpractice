<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Time Out</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />

</head>
<body class="pentaho-page-background" style="margin:0px;">

	<div id="container">
            <div style="padding: 30px 30px 30px 30px; margin: 40px 0 0 100px; position: relative;">
            <label style="font-size: 3.0em;">Time Out</label><label style="font-size: 1.5em;"></label><br/>
		</div>
	</div>
</body>
</html>