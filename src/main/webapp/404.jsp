<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Not Found</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/crr.css" />

</head>
<body class="pentaho-page-background" style="margin: 0px;">

    <div id="container">
        <div style="padding: 30px 30px 30px 30px; margin: 40px 0 0 100px; position: relative;">
            <label style="font-size: 3.0em;">Not Found</label><label style="font-size: 1.5em;"></label><br /> <label style="font-size: 1.6em; line-height: 30px;"> <span>404:指定されたページが見つかりません。</span>
            </label>
        </div>
    </div>
</body>
</html>